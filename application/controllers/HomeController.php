<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Home/HomeModel');
        $this->load->library('session');
        $this->load->helper('url');
    } 

	public function index()
	{
		if(!isset($_SESSION['nombre'])){
			$this->load->view('Home/Login');
		}
		else
		{
			$data['title'] = "Inicio";
			$data['view'] = 'Home/Index';
			$this->load->view('layout/Index', $data);
		}
	}

	public function Login()
	{
		$this->load->view('Home/Login');
	}

	public function iniciaSesion()
	{
		$datos = $this->input->post();
		$nombre = $datos['nombre'];
		$password = md5($datos['password']);
		$sesiones = $this->HomeModel->ValidaUsuario($nombre, $password);
		if(!$sesiones){
			$this->session->set_flashdata('message', 'Usuario o contraseña erredos, verifique!');
			redirect('/');
		}
		else{
			foreach ($sesiones as $key => $value) {
				$_SESSION[$key] = $value;
			}
			redirect('/');
		}
	}

	public function CierraSesion()
	{
		session_destroy();
		// $this->load->view('Home/Login');
		redirect('Login');

	}
}

