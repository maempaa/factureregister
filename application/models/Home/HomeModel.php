<?php

class HomeModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ciudad by idCiudad
     */
    function ValidaUsuario($nombre, $password)
    {
        return $this->db->get_where('usuarios', array('nombre'=>$nombre, 'password'=>$password))->row_array();
    }
    
    /*
     * Get all ciudades count
     */
    function get_all_ciudades_count()
    {
        $this->db->from('ciudades');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all ciudades
     */
    function get_all_ciudades($params = array())
    {
        $this->db->order_by('idCiudad', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('ciudades')->result_array();
    }
        
    /*
     * function to add new ciudad
     */
    function add_ciudad($params)
    {
        $this->db->insert('ciudades',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ciudad
     */
    function update_ciudad($idCiudad,$params)
    {
        $this->db->where('idCiudad',$idCiudad);
        return $this->db->update('ciudades',$params);
    }
    
    /*
     * function to delete ciudad
     */
    function delete_ciudad($idCiudad)
    {
        return $this->db->delete('ciudades',array('idCiudad'=>$idCiudad));
    }
}
