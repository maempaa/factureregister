<?php
include "Header.php";
include "Aside.php";
?>

    <div class="page-wrapper">
        <div class="container-fluid">
            <!-- Cargar la tra vista -->
            <?php if(isset($view))
                $this->load->view($view);
            ?>
        </div>
        <footer class="footer text-center">
            Todos los derechos reservados Innovo Plaza centro comrecial y de oficinas<a href="innovoplaza.com"></a>.
        </footer>
    </div>
</div>

<?php
include "Footer.php";
?>